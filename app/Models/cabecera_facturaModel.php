<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cabecera_facturaModel extends Model
{
   protected $table='cabecerafactura';
   protected $primaryKey = 'serial_caf';
   public $timestamps = false;


    public function instalacion()
    {
        return $this->belongsTo('App\Models\instalacionModel','serial_ins');
    }

    public function detalles()
    {
        return $this->hasMany('App\Models\detalle_facturaModel','serial_caf');
    }
}
