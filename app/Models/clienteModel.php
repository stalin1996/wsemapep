<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class clienteModel extends Model
{
   protected $table='cliente';
   protected $primaryKey = 'serial_cli';
   public $timestamps = false;


    public function instalaciones()
    {
        return $this->hasMany('App\Models\instalacionModel','serial_cli');
    }
}
