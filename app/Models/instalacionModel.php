<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class instalacionModel extends Model
{
   protected $table='instalacion';
   protected $primaryKey = 'serial_ins';
   public $timestamps = false;

    public function catastro()
    {
        return $this->belongsTo('App\Models\catastroModel','serial_cat');
    }

       public function cliente()
    {
        return $this->belongsTo('App\Models\clienteModel','serial_cli');
    }
}
