<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tarifaModel extends Model
{
   protected $table='tarifas';
   protected $primaryKey = 'serial_tar';
   public $timestamps = false;


    public function cabecera()
    {
        return $this->belongsTo('App\Models\cabecera_tarifaModel','serial_ctar');
    }


}
